import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Homework 1'),
        ),
        body: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: 'Name: Pedro Suazo\n\n',
            style: new TextStyle(
              fontSize: 14.0,
              color: Colors.black,
            ),
            children: <TextSpan>[
              TextSpan(text: 'Graduation Date: May 2021\n\n'),
              TextSpan(text: 'Favorite Quote: '),
              TextSpan(text: '\"I need to be myself, I can\'t be no one else\"\n', style: new TextStyle(fontStyle: FontStyle.italic)),
              TextSpan(text: '-Liam Gallagher'),
            ],
          ),
        )
      ),
    );
  }
}